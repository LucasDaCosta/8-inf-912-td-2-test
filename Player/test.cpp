﻿#include <iostream>
#include "Serializer.h"
#include "Deserializer.h"
#include "Compression.hpp"
#include "Decompression.hpp"

using namespace std;
using namespace uqac::compression;
using namespace uqac::serialize;

void testSerialization() {
	// String on first computer
	const char name[128] = "Aymeric";
	int power = 2;
	pair<int, int> powerRange(0, 10);
	int wisdom = 300;
	pair<int, int> wisdomRange(0, 700);
	int magic = 75535;
	pair<int, int> magicRange(0, 80000);

	int fish = -2;
	pair<int, int> fishRange(-10, 10);
	int boar = -300;
	pair<int, int> boarRange(-400, 400);
	int mana = -75535;
	pair<int, int> manaRange(-80000, 80000);



	float food = 50.4;
	pair<float, float> foodRange(0, 100);
	float health = -50.4;
	pair<float, float> healthRange(-100, 100);

	Compression compressionTool;


	compressionTool.compressString(name);
	compressionTool.compressIntWithBytePrecision(power, powerRange);
	compressionTool.compressIntWithBytePrecision(wisdom, wisdomRange);
	compressionTool.compressIntWithBytePrecision(magic, magicRange);
	compressionTool.compressFloatWithBytePrecision(food, foodRange, 10);
	compressionTool.compressIntWithBytePrecision(fish, fishRange);
	compressionTool.compressIntWithBytePrecision(boar, boarRange);
	compressionTool.compressIntWithBytePrecision(mana, manaRange);
	compressionTool.compressFloatWithBytePrecision(health, healthRange, 10);

	// String on the second computer 
	char newName[128] = {};
	std::string buffer = compressionTool.getSerializerContent();

	cout << "Our current buffer is" << buffer << endl;

	// Begin decomrpession
	Decompression decompressionTool(buffer.c_str(), buffer.size());

	cout << "---- Test on strings ----" << endl;
	decompressionTool.decompressString(newName);
	cout << "Our decompressed name is " << newName << endl;

	cout << "---- Test on positive ints ----" << endl;
	int newPower = decompressionTool.decompressIntWithBytePrecision(powerRange);
	int newWisdom = decompressionTool.decompressIntWithBytePrecision(wisdomRange);
	int newMagic = decompressionTool.decompressIntWithBytePrecision(magicRange);
	cout << "Our decompressed power is " << newPower << ". Expected " << power << endl;
	cout << "Our decompressed wisdom is " << newWisdom << ". Expected " << wisdom << endl;
	cout << "Our decompressed magic is " << newMagic << ". Expected " << magic << endl;

	cout << "---- Test on positive float ----" << endl;
	float newFood = decompressionTool.decompressFloatWithBytePrecision(foodRange, 10);
	cout << "Our decompressed food is " << newFood << ". Expected " << food << endl;

	cout << "---- Test on negative ints ----" << endl;
	int newFish = decompressionTool.decompressIntWithBytePrecision(fishRange);
	int newBoar = decompressionTool.decompressIntWithBytePrecision(boarRange);
	int newMana = decompressionTool.decompressIntWithBytePrecision(manaRange);
	cout << "Our decompressed fish is " << newFish << ". Expected " << fish << endl;
	cout << "Our decompressed boar is " << newBoar << ". Expected " << boar << endl;
	cout << "Our decompressed mana is " << newMana << ". Expected " << mana << endl;

	cout << "---- Test on negative floats ----" << endl;
	float newHealth = decompressionTool.decompressFloatWithBytePrecision(healthRange, 10);
	cout << "Our decompressed health is " << newHealth << ". Expected " << health << endl;
}
	

/*
void testIntByteCompression() {
	int sun = 40;
	std::pair<int, int> sunRange(0, 50);

	int moon = 270;
	std::pair<int, int> moonRange(0, 300);

	int galaxy = 75535;
	std::pair<int, int> galaxyRange(0, 80000);

	Compression compressionTool;
	Decompression decompressionTool;

	void* buffer = compressionTool.compressIntWithBytePrecision(sun, sunRange);
	int newSun = decompressionTool.decompressIntWithBytePrecision(buffer, sunRange);

	cout << "---- Testing int compression on bytes - char compression ----" << endl;
	cout << "My newSun is " << newSun << ". Expected " << sun << endl;

	buffer = compressionTool.compressIntWithBytePrecision(moon, moonRange);
	int newMoon = decompressionTool.decompressIntWithBytePrecision(buffer, moonRange);

	cout << "---- Testing int compression on bytes - short compression ----" << endl;
	cout << "My newMoon is " << newMoon << ". Expected " << moon << endl;

	cout << "---- Testing int compression on bytes - int compression ----" << endl;
	buffer = compressionTool.compressIntWithBytePrecision(galaxy, galaxyRange);
	if (buffer == nullptr) {
		cout << "Unable to compress : int to big" << endl;
	}
}
*/

void antoineTest() {
	uqac::serialize::Serializer mySerializer(5);
	char* test1 = "Test1\0";
	char* test2 = "Ceci est un test 2 un peu plus long\0";
	int test3 = 888;
	double test4 = 888.888;
	mySerializer.serialize<char*>(test1);
	mySerializer.serialize<char*>(test2);
	mySerializer.serialize<int>(test3);
	mySerializer.serialize<float>(test4);
	cout << "Values : " << test1 << endl << test2 << endl << test3 << endl << test4 << endl;

	string strTemp = mySerializer.getContainer();
	uqac::serialize::Deserializer myDeserializer(strTemp.c_str(), strTemp.size());

	cout << "String from serializer: " << strTemp << endl;

	char buffer[255];
	myDeserializer.read(buffer);
	std::cout << "Deserialized value : " << buffer << std::endl;
	myDeserializer.read(buffer);
	std::cout << "Deserialized value : " << buffer << std::endl;
	myDeserializer.read(buffer);
	std::cout << "Deserialized value : " << buffer << std::endl;
	myDeserializer.read(buffer);
	std::cout << "Deserialized value : " << buffer << std::endl;
}










