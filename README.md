# 8INF912 - Sujet spécial en informatique II - Programmation réseau dans les jeux vidéo

__Auteurs :__

- Antoine BOULANGE - BOUA08049704
- Aymeric FERRON - FERA19039907
- Léonard RIZZO - RIZA24029900

## TP2 - Sérialisation et compression

Pratiquer les techniques de Sérialisation et de Compression, ainsi que les templates.

__Travail effectué :__

Une librairie de sérialisation et de compression a été développée dans le dossier `TD2/TD2/Library`. Elle contient notamment une classe _Compression_ et une classe _Decompression_ qui peuvent être utilisées afin de manipuler des _int_, des _float_, des _Vector3_ et des _Quaternions_.

La classe _Compression_ contient une instance de la classe _Serializer_ et la classe _Decompression_ possède une instance de la classe _Deserializer_. Ces instances permettent de sérialiser des _int_, des _float_, des _Vector3_ et des _Quaternion_ en les stockant les uns à la suite des autres sous la forme de _char_. 

Des tests ont été développés dans le fichier `TD2/TD2/Player/Player.cpp`, conformément au sujet du TP. Le projet se compile avec CMake grâce au fichier `TD2/TD2/CMakeLists.txt`. Les exécutables sont générés dans le dossier `/TD2/out/build/x64-Debug/TD2/Player`.

