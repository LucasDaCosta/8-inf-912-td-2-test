cmake_minimum_required(VERSION 3.8)

set(SerializeLib ${Version})

add_library(Serialize STATIC "src/Deserializer.cpp" "src/Serializer.cpp" "src/Compression.cpp" "src/Framework.cpp" "include/Decompression.hpp" "src/Decompression.cpp")

target_include_directories(Serialize PUBLIC include)