#pragma once
#include <utility>
#include "Deserializer.h"
#include "Framework.h"

namespace uqac::compression {
	class Decompression {

	private:
		uqac::serialize::Deserializer myDeserializer;

	public:

		Decompression() = default;
		Decompression(const char* buffer, int bufferSize);
		~Decompression() = default;

		/*
			Function that decompress a char* string.
		
			@param buffer is the buffer where comrpessed data are written. 
			First byte is used to store size of the usefull data
		*/
		void decompressString(char* buffer);

		/*
			Decompress a positive-natural number and return an int.
			Seek the int in the myDeserializer.

			@param range is the range of the int to decompress
		*/
		int decompressIntWithBytePrecision(std::pair<int, int> range);

		/*
			Decompress a positive-natural number and return a float.
			Seek the float in the myDeserializer.

			@param range is the range of the float to decompress
			@param precision is the precision required for the float

			@return the float deserialized and decompressed
		*/
		float decompressFloatWithBytePrecision(std::pair<float, float> range, int precision);

		/*
			Deserialize and decompress a Vector3<float>

			@param firstRange is the range of the first float of the vector
			@param secondRange is the range of the second float of the vector
			@param thirdRange is the range of the third float of the vector
			@param precision is the precision required for the compression

			@return the Vector3<float> deserialized and decompressed
		*/
		uqac::utils::Vector3<float> decompressVector3Float(
			std::pair<float, float> firstRange,
			std::pair<float, float> secondRange,
			std::pair<float, float> thirdRange,
			int precision);

		/*
			Deserialize and decompress a Vector3<int>

			@param firstRange is the range of the first int of the vector
			@param secondRange is the range of the second int of the vector
			@param thirdRange is the range of the third int of the vector

			@return the Vector3<int> deserialized and decompressed
		*/
		uqac::utils::Vector3<int> decompressVector3Int(
			std::pair<int, int> firstRange,
			std::pair<int, int> secondRange,
			std::pair<int, int> thirdRange);

		/*
			Deserialize and decompress a Quaternion

			@param precision is the precision required for the compression

			@return the Quaternion deserialized and decompressed
		*/
		uqac::utils::Quaternion decompressQuaternion(int precision);

	private:
		/*
			Utility from Mark Ransom : https://stackoverflow.com/questions/21191307/minimum-number-of-bits-to-represent-a-given-int
			Get the nb of bit used by an int
		*/
		int bitsNeeded(int value);

		/*
			Function that return a int sliced into several char.

			@param buffer is the buffer containing sliced int
			@param int size is the size of the int hidden in the buffer
		*/
		unsigned int stringToInt(char* buffer, int size);

		/*
			Decompress two float hidden in a single int

			@param buffer is the int where floats are compressed
			@param first is the buffer where first float will be written
			@param firstRange is the range of the first float
			@param second is the second buffer where second float will be written
			@param secondRange is the range of the second float
		*/
		void decompressFloatWithBitPrecision(unsigned int buffer,
			float* first, std::pair<float, float> firstRange,
			float* second, std::pair<float, float> secondRange);

		/*
			Decompress two int hidden in a single int

			@param buffer is the int where ints are compressed
			@param first is the buffer where first int will be written
			@param firstRange is the range of the first int
			@param second is the second buffer where second int will be written
			@param secondRange is the range of the second int
		*/
		void decompressIntWithBitPrecision(unsigned int buffer,
			int* first, std::pair<int, int> firstRange,
			int* second, std::pair<int, int> secondRange);
	};
}