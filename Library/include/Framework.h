#pragma once

#include <memory>
#include <iostream>

namespace uqac::utils {
	template <class T>
	struct Vector3 {
		T x;
		T y;
		T z;

		inline Vector3(const T _x, const T _y, const T _z) : 
			x{ _x }, y{ _y }, z{ _z } {}
		inline Vector3(const std::shared_ptr<Vector3> toCopy) : 
			x{ toCopy->x }, y{ toCopy->y }, z{ toCopy->z } {}
		~Vector3() = default;

		friend inline std::ostream& operator<<(std::ostream& os, const Vector3<T>& v) {
			os << "(" << v.x << ", " << v.y << ", " << v.z << ")";
			return os;
		}
	};

	struct Quaternion {
		Vector3<float> eulerAngles;
		float w;

		inline Quaternion(const float _x, const float _y, const float _z, const float _w) : 
			eulerAngles{ Vector3(_x, _y, _z) }, w{ _w } {}
		inline Quaternion(const Vector3<float> v, const float _w) :
			eulerAngles{ v }, w{ _w } {}
		inline Quaternion(const std::shared_ptr<Quaternion> toCopy) : 
			eulerAngles{ Vector3(toCopy->eulerAngles) }, w{ toCopy->w } {}
		~Quaternion() = default;

		friend inline std::ostream& operator<<(std::ostream& os, const Quaternion& q) {
			os << "(" << q.eulerAngles << ", " << q.w << ")";
			return os;
		}
	};
}