#include <iostream>
#include <vector>
#include <string.h>
#include "Deserializer.h"

namespace uqac::serialize {

	Deserializer::Deserializer(const char* buffer, int buffsize) {
		this->position = 0;
		int positionTemp = 0;
		int currentLength = 0;
		this->buffer = *(new std::vector<char>(buffsize));

		while (positionTemp < buffsize) {
			this->buffer.at(positionTemp) = *buffer;
			buffer++;
			positionTemp++;
		}
	}

	/*
	return the argument located at argumentIndex (indices range from 0 to argumentSize-1) and return it in buffer, returning its size
	*/
	int Deserializer::read(char* buffer) {
		if (position >= this->buffer.size()) {
			std::cout << "[Deserializer] Nothing left to deserialize." << std::endl;
			return -1;
		}

		size_t currentSize = 0;

		currentSize = this->buffer.at(position); //get the length of the current index's argument
		currentSize += 128; //adds 128 to get back to the original int

		std::string str;
		position++;

		for (int i = 0; i < currentSize; i++) {
			str.push_back(this->buffer.at(position));
			position++;
		}

		strcpy(buffer, str.c_str());

		return currentSize;
	}
}