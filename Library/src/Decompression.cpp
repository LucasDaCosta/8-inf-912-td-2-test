#include "../include/Decompression.hpp"
#include <math.h> 



namespace uqac::compression {

	Decompression::Decompression(const char* buffer, int bufferSize)
	{
		this->myDeserializer = uqac::serialize::Deserializer(buffer, bufferSize);
	}
	
	void Decompression::decompressString(char* buffer) {
		this->myDeserializer.read(buffer);
	}

	void Decompression::decompressFloatWithBitPrecision(unsigned int buffer,
										float* first, std::pair<float, float> firstRange,
										float* second, std::pair<float, float> secondRange) {
		float multiplicator = 100.00;
		int firstInt = 0;
		int secondInt = 0;
		std::pair<int, int> firstRangeInt(firstRange.first * multiplicator, firstRange.second * multiplicator);
		std::pair<int, int> secondRangeInt(secondRange.second * multiplicator, secondRange.second * multiplicator);

		this->decompressIntWithBitPrecision(buffer, &firstInt, firstRangeInt, &secondInt, secondRangeInt);


		*first = firstInt / multiplicator;
		*second = secondInt / multiplicator;
	}

	void Decompression::decompressIntWithBitPrecision(unsigned int buffer,
										int* first, std::pair<int, int> firstRange,
										int* second, std::pair<int, int> secondRange) {
		// Get second value 
		*second = buffer;

		// Force all bytes about the first int value to 0
		// Get the nb of bits allocated for second int
		int bitsForSecondInt = 0;
		if (secondRange.first < 0) {
			bitsForSecondInt = this->bitsNeeded(secondRange.second + -1 * (secondRange.first));
		}
		else {
			bitsForSecondInt = this->bitsNeeded(secondRange.second);
		}

		for (int i = bitsForSecondInt; i < 32; i++) {
			*second = *second & ~(1 << i);
		}
		 
		// Get first int value
		*first = buffer;
		// Swift bits which belong to second int
		*first = *first >> bitsForSecondInt;
	}

	int Decompression::decompressIntWithBytePrecision(std::pair<int, int> range) {
		int finalValue = 0;
		char myCharValue[4] = {};

		int myValueSize = this->myDeserializer.read(myCharValue);

		finalValue = this->stringToInt(myCharValue, myValueSize);

		if (range.first < 0) {
			finalValue -= range.second;
		}
		return finalValue;
	}

	uqac::utils::Vector3<float> Decompression::decompressVector3Float(
												std::pair<float, float> firstRange,
												std::pair<float, float> secondRange,
												std::pair<float, float> thirdRange,
												int precision) {
		float x = this->decompressFloatWithBytePrecision(firstRange, precision);
		float y = this->decompressFloatWithBytePrecision(secondRange, precision);
		float z = this->decompressFloatWithBytePrecision(thirdRange, precision);
		return uqac::utils::Vector3<float>(x, y, z);
	}

	uqac::utils::Vector3<int> Decompression::decompressVector3Int(
											std::pair<int, int> firstRange,
											std::pair<int, int> secondRange,
											std::pair<int, int> thirdRange) {
		int x = this->decompressIntWithBytePrecision(firstRange);
		int y = this->decompressIntWithBytePrecision(secondRange);
		int z = this->decompressIntWithBytePrecision(thirdRange);
		return uqac::utils::Vector3<int>(x, y, z);
	}

	uqac::utils::Quaternion Decompression::decompressQuaternion(int precision) {
		unsigned int pow2 = pow(2, 2);
		unsigned int pow10 = pow(2, 10);

		std::pair<int, int> range(-pow(2, 31), (pow(2, 31) - 1));

		int containerTemp = this->decompressIntWithBytePrecision(range);

		unsigned int container = containerTemp + pow(2, 31);

		unsigned int forgottenIndex = container - (container / pow2) * pow2;

		container /= pow2;

		unsigned int i = 0;

		std::vector<float> myRes(4);

		float forgottenFloat = 1;

		while (i < 4) {
			if (i != forgottenIndex) {
				myRes.at(i) = ((container - (container / pow10) * pow10) / 0.724 - 707)/1000;
				forgottenFloat -= pow(myRes.at(i),2);
				container /= pow10;
			}
			i++;
		}

		myRes.at(forgottenIndex) = sqrt(forgottenFloat);

		return uqac::utils::Quaternion(uqac::utils::Vector3(myRes.at(0), myRes.at(1), myRes.at(2)), myRes.at(3));
	}

	float Decompression::decompressFloatWithBytePrecision(std::pair<float, float> range, int precision) {
		float finalValue = 0.0;
		int precisionMultiple = pow(10, precision);

		std::pair<int, int> intRange(range.first * precisionMultiple, range.second * precisionMultiple);
		
		float myFloatIsAnInt = this->decompressIntWithBytePrecision(intRange);

		finalValue = myFloatIsAnInt / precisionMultiple;

		return finalValue;
	}

	unsigned int Decompression::stringToInt(char* buffer, int size) {
		unsigned int myInt = 0;
		unsigned int positionPower = 1;
		unsigned int power = pow(2, 8);
		unsigned int cache = 0;

		for (int i = 0; i < size;  i++) {
			cache = buffer[i];
			cache += 128;
			myInt += cache * positionPower;
			positionPower *= power;
		}

		return myInt;
	}

	int Decompression::bitsNeeded(int value)
	{
		int bits = 0;
		if (value >= 0x10000)
		{
			bits += 16;
			value >>= 16;
		}
		if (value >= 0x100)
		{
			bits += 8;
			value >>= 8;
		}
		if (value >= 0x10)
		{
			bits += 4;
			value >>= 4;
		}
		if (value >= 0x4)
		{
			bits += 2;
			value >>= 2;
		}
		if (value >= 0x2)
		{
			bits += 1;
			value >>= 1;
		}
		return bits + value;
	}
}